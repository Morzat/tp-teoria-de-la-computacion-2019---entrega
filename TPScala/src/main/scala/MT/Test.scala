package MT


import com.teoriaDeLaOrganizacion._
import org.scalactic.source.Position.apply

class Test extends UnitSpec {
  
    "Ejemplo Diapositivas: Al correr maquina "must " quedar en: Estado:f | Cinta (B, B, 0, 0, 1, B) | Posicion Actual: 4 "in {
      val estado=new Estado("q",false)  
      val estadoFinal=new Estado("f",true)
      
      val transiciones= Map.apply((estado, '0')-> (estado, '0', new Direccion('R')),
                                  (estado, '1')-> (estadoFinal, '0', new Direccion('R')),
                                  (estado, 'B')-> (estado, '1', new Direccion('L')))
                                  
                               
                          
      val maquina=new MaquinaTuring(transiciones) 
      
      val listaChar=List('B', 'B', '0', 'B','1','B') 
      val cinta=new Cinta(listaChar,2)
      assert(maquina.procesar(estado,cinta).toString()=="(Estado actual: <f> y es final,Lista: List(B, B, 0, 0, 1, B) | Posicion Actual: 4)")
   }
    
    "Ejemplo Complemento numero binario: Al correr maquina "must " quedar en: Estado:f | Cinta (0, 1, 1, 0, 0, 0, B) | Posicion Actual: 5 "in {
     val estado=new Estado("q",false)  
     val estadoFinal=new Estado("f",true)
     val transiciones2= Map.apply((estado, '0')-> (estado, '1', new Direccion('R')),
                                  (estado, '1')-> (estado, '0', new Direccion('R')),
                                  (estado, 'B')-> (estadoFinal, 'B', new Direccion('L')))
                                
                            
                        
     val maquina2=new MaquinaTuring(transiciones2)
      
     val listaChar2=List('1', '0', '0', '1','1','1','B') 
     val cinta2=new Cinta(listaChar2,0) 
    
     assert(maquina2.procesar(estado,cinta2).toString()=="(Estado actual: <f> y es final,Lista: List(0, 1, 1, 0, 0, 0, B) | Posicion Actual: 5)")
   }
    
     //SOLO FUNCIONA CON NUMEROS BINARIOS DE 4 BITS Y EL RESULTADO SE REFLEJA LUEGO DEL SEGUNDO '|'

    "Ejemplo suma binaria entre dos numeros (14+15) con cuatro bits: Al correr maquina "must " quedar en: Estado:f | Cinta (B, X, X, X, X, |, B, X, X, X, X, B, |, 1, 1, 1, 0, 1) | Posicion Actual: 4 "in {
     val estadoNum1=new Estado("LeyendoNum1",false)
     val estadoNum2=new Estado("LeyendoNum2",false)
     val volviendo=new Estado("Volviendo a buscar numero",false)
     val estadoMoverNum0=new Estado("Moviendo nuevo numero 0 en posicion",false)
     val estadoMoverNum1=new Estado("Moviendo nuevo numero 1 en posicion",false)
     val estadoDejarNum0=new Estado("Dejando nuevo numero en posicion",false)
     val estadoDejarNum1=new Estado("Dejando nuevo numero en posicion",false)
     val estadoMover0Num2=new Estado("Sumando 0",false)
     val estadoMover1Num2=new Estado("Sumando 1",false)
     
     val sumar0=new Estado("Sumando bit con 0",false)
     val sumar1=new Estado("Sumando bit con 1",false)
     val acarreo1=new Estado("Acarreo 1",false)
     
     val volviendoNum2=new Estado ("Vuelvo a buscar bits de num2",false)
     
     val estadoBuscarNum2=new Estado("Busco donde esta el numero 2 en la cinta",false)
     val estadoBuscarInicioNum2=new Estado("Busco Inicio del numero 2",false)
     val buscarBarraFinal=new Estado("Busco barra final para moverla",false)
     val reacomodarBarraFinal=new Estado("Reacomodo barra final",false)
     
     val nuevo0=new Estado("Pongo un 0 luego de la barra",false)
     val nuevo1=new Estado("Pongo un 1 luego de la barra",false)
     val nuevoB=new Estado("Pongo un B luego de la barra",false)
     
     val estadoFinal=new Estado("f",true)
     
     val transiciones= Map.apply(
                                 (estadoNum1, 'X')-> (estadoNum1, 'X', new Direccion('L')),
                                 (estadoNum1, '0')-> (estadoMoverNum0, 'X', new Direccion('R')), //marca X para saber que ya movi ese numero y no debo leerlo nuevamente
         
                                 (estadoMoverNum0, '0')-> (estadoMoverNum0, '0', new Direccion('R')),
                                 (estadoMoverNum0, '1')-> (estadoMoverNum0, '1', new Direccion('R')),
                                 (estadoMoverNum0, 'B')-> (estadoMoverNum0, 'B', new Direccion('R')),
                                 (estadoMoverNum0, 'X')-> (estadoMoverNum0, 'X', new Direccion('R')),
                                 (estadoMoverNum0, '/')-> (estadoMoverNum0, '/', new Direccion('R')),
                                 (estadoMoverNum0, '-')-> (estadoMoverNum0, '-', new Direccion('R')),//  '-' INDICA QUE SE RECORRIO EL NUMERO 2
                                 (estadoMoverNum0, '|')-> (estadoDejarNum0, '|', new Direccion('L')), // '|' para indicar que ahi debe terminar el resultado
                                  
                                 (estadoDejarNum0,'B')->(volviendo,'0',new Direccion('L')), 
                                 (estadoDejarNum0,'1')->(estadoDejarNum0,'1',new Direccion('L')), 
                                 (estadoDejarNum0,'0')->(estadoDejarNum0,'0',new Direccion('L')), 
                                  
                                 (volviendo,'B')->(volviendo,'B',new Direccion('L')),
                                 (volviendo,'1')->(volviendo,'1',new Direccion('L')),
                                 (volviendo,'0')->(volviendo,'0',new Direccion('L')),
                                 (volviendo,'-')->(volviendo,'-',new Direccion('L')),
                                 (volviendo,'X')->(estadoNum1,'X',new Direccion('L')),
                                 (volviendo,'/')->(volviendo,'/',new Direccion('L')),
                                  
                                  
                                 (estadoNum1, '1')-> (estadoMoverNum1, 'X', new Direccion('R')),
                                  
                                 (estadoMoverNum1, '0')-> (estadoMoverNum1, '0', new Direccion('R')),
                                 (estadoMoverNum1, '1')-> (estadoMoverNum1, '1', new Direccion('R')),
                                 (estadoMoverNum1, 'B')-> (estadoMoverNum1, 'B', new Direccion('R')),
                                 (estadoMoverNum1, '|')-> (estadoDejarNum1, '|', new Direccion('L')),
                                 (estadoMoverNum1, '/')-> (estadoMoverNum1, '/', new Direccion('R')),
                                 (estadoMoverNum1, 'X')-> (estadoMoverNum1, 'X', new Direccion('R')),
                                  
                                 (estadoDejarNum1,'B')->(volviendo,'1',new Direccion('L')), 
                                 (estadoDejarNum1,'1')->(estadoDejarNum1,'1',new Direccion('L')), 
                                 (estadoDejarNum1,'0')->(estadoDejarNum1,'0',new Direccion('L')), 
                                  
                                 (estadoNum1, 'B')-> (estadoBuscarNum2, 'B', new Direccion('R')),
                                  
                                 
                                  
                                  
                                 (estadoBuscarNum2, '0')-> (estadoBuscarNum2, '0', new Direccion('R')),
                                 (estadoBuscarNum2, '1')-> (estadoBuscarNum2, '1', new Direccion('R')),
                                 (estadoBuscarNum2, 'B')-> (estadoBuscarNum2, 'B', new Direccion('R')),
                                 (estadoBuscarNum2, '|')-> (estadoBuscarNum2, '|', new Direccion('L')),
                                 (estadoBuscarNum2, '/')-> (estadoBuscarInicioNum2, '-', new Direccion('R')),//- para marcar fin de num 2
                                 (estadoBuscarNum2, 'X')-> (estadoBuscarNum2, 'X', new Direccion('R')),
                                  
                                  
                                 (estadoBuscarInicioNum2, '0')-> (estadoBuscarInicioNum2, '0', new Direccion('R')),
                                 (estadoBuscarInicioNum2, '1')-> (estadoBuscarInicioNum2, '1', new Direccion('R')),
                                 (estadoBuscarInicioNum2, 'B')-> (estadoBuscarInicioNum2, 'B', new Direccion('R')),
                                 (estadoBuscarInicioNum2, '|')-> (estadoBuscarInicioNum2, '|', new Direccion('L')),
                                 (estadoBuscarInicioNum2, '/')-> (estadoNum2, '/', new Direccion('L')),
                                 (estadoBuscarInicioNum2, 'X')-> (estadoBuscarInicioNum2, 'X', new Direccion('R')),
                                  
                                 (estadoNum2, 'X')-> (estadoNum2, 'X', new Direccion('L')),
                                 (estadoNum2, 'B')-> (estadoNum2, 'B', new Direccion('L')),
                                 (estadoNum2, '0')-> (estadoMover0Num2, 'X', new Direccion('R')),
                                 (estadoNum2, '-')-> (buscarBarraFinal, 'F', new Direccion('R')),
                                 (estadoNum2, 'F')-> (estadoFinal,'|',new Direccion('L')),
                                  
                                 (estadoMover0Num2, '0')-> (estadoMover0Num2, '0', new Direccion('R')),
                                 (estadoMover0Num2, '1')-> (estadoMover0Num2, '1', new Direccion('R')),
                                 (estadoMover0Num2, 'B')-> (estadoMover0Num2, 'B', new Direccion('R')),
                                 (estadoMover0Num2, 'X')-> (estadoMover0Num2, 'X', new Direccion('R')),
                                 (estadoMover0Num2, '/')-> (estadoMover0Num2, '/', new Direccion('R')),
                                 (estadoMover0Num2, '|')-> (sumar0, '|', new Direccion('L')),
                                 
                                 (sumar0, '0')-> (buscarBarraFinal, '0', new Direccion('R')),
                                 (sumar0, '1')-> (buscarBarraFinal, '1', new Direccion('R')),  
                                 (sumar0, 'B')-> (buscarBarraFinal, '0', new Direccion('R')),
                                  
                                 (buscarBarraFinal, '0')-> (buscarBarraFinal, '0', new Direccion('R')),
                                 (buscarBarraFinal, '1')-> (buscarBarraFinal, '1', new Direccion('R')),
                                 (buscarBarraFinal, 'B')-> (buscarBarraFinal, 'B', new Direccion('R')),
                                 (buscarBarraFinal, '/')-> (buscarBarraFinal, 'B', new Direccion('R')),
                                 (buscarBarraFinal, 'X')-> (buscarBarraFinal, 'X', new Direccion('R')),
                                 (buscarBarraFinal, '|')-> (reacomodarBarraFinal, '|', new Direccion('L')),
                                  
                                 (reacomodarBarraFinal, '0')-> (nuevo0, '|', new Direccion('R')),
                                 (reacomodarBarraFinal, '1')-> (nuevo1, '|', new Direccion('R')),
                                 (reacomodarBarraFinal, 'B')-> (nuevoB, '|', new Direccion('R')),
                                  
                                 (nuevo0, '|')-> (volviendoNum2, '0', new Direccion('L')),
                                 
                                 (nuevo1, '|')-> (volviendoNum2, '1', new Direccion('L')),
                                 
                                 (nuevoB, '|')-> (volviendoNum2, 'B', new Direccion('L')),
                                  
                                  
                                 
                                 (estadoNum2, '1')-> (estadoMover1Num2, 'X', new Direccion('R')),
                                  
                                 (estadoMover1Num2, '0')-> (estadoMover1Num2, '0', new Direccion('R')),
                                 (estadoMover1Num2, '1')-> (estadoMover1Num2, '1', new Direccion('R')),
                                 (estadoMover1Num2, 'B')-> (estadoMover1Num2, 'B', new Direccion('R')),
                                 (estadoMover1Num2, 'X')-> (estadoMover1Num2, 'X', new Direccion('R')),
                                 (estadoMover1Num2, '/')-> (estadoMover1Num2, '/', new Direccion('R')),
                                 (estadoMover1Num2, '|')-> (sumar1, '|', new Direccion('L')),
                                  
                                 (sumar1, '0')-> (buscarBarraFinal, '1', new Direccion('L')),
                                 (sumar1, '1')-> (acarreo1, '0', new Direccion('L')),
                                 (sumar1, 'B')-> (buscarBarraFinal, '1', new Direccion('L')),
                                  
                                 (acarreo1, '0')-> (buscarBarraFinal, '1', new Direccion('L')),
                                 (acarreo1, '1')-> (acarreo1, '0', new Direccion('L')),
                                 (acarreo1, 'B')-> (buscarBarraFinal, '1', new Direccion('L')),
                                  
                                 (volviendoNum2,'B')->(volviendoNum2,'B',new Direccion('L')),
                                 (volviendoNum2,'1')->(volviendoNum2,'1',new Direccion('L')),
                                 (volviendoNum2,'0')->(volviendoNum2,'0',new Direccion('L')),
                                 (volviendoNum2,'/')->(volviendoNum2,'/',new Direccion('L')),
                                 (volviendoNum2,'|')->(volviendoNum2,'|',new Direccion('L')),
                                 (volviendoNum2,'X')->(estadoNum2,'X',new Direccion('L')),
                                 (volviendoNum2,'-')->(buscarBarraFinal,'F',new Direccion('R')), //al encontrar F la MT cambia a estado Final
                                 (volviendoNum2,'F')->(estadoFinal,'/',new Direccion('L')),
                                  
                                  
                                 //las transiciones siguientes son inalcanzables por esos estados pero son necesarias para que la MT sea deterministica 
                                 (estadoNum1, '/')-> (estadoNum1, '/', new Direccion('R')),                                                                                     
                                 (estadoNum1, '|')-> (estadoNum1, '|', new Direccion('R')), 
                                 (estadoNum1, '-')-> (estadoNum1, '-', new Direccion('R')), 
                                 (estadoNum1, 'F')-> (estadoNum1, 'F', new Direccion('R')),
                                 
                                 (estadoNum2, '|')-> (estadoNum2, '|', new Direccion('R')), 
                                 (estadoNum2, '/')-> (estadoNum2, '/', new Direccion('R')), 
                                 
                                 (volviendo,'|')->(volviendo,'|',new Direccion('R')),
                                 (volviendo,'F')->(volviendo,'F',new Direccion('R')),
                                 
                                 (estadoMoverNum0, 'F')-> (estadoMoverNum0, 'F', new Direccion('R')),
                                 
                                 (estadoMoverNum1, 'F')-> (estadoMoverNum1, 'F', new Direccion('R')),
                                 (estadoMoverNum1, '-')-> (estadoMoverNum1, '-', new Direccion('R')),
                                 
                                 (estadoDejarNum0,'|')->(estadoDejarNum0,'|',new Direccion('R')), 
                                 (estadoDejarNum0,'F')->(estadoDejarNum0,'F',new Direccion('R')), 
                                 (estadoDejarNum0,'/')->(estadoDejarNum0,'/',new Direccion('R')), 
                                 (estadoDejarNum0,'-')->(estadoDejarNum0,'-',new Direccion('R')), 
                                 (estadoDejarNum0,'X')->(estadoDejarNum0,'X',new Direccion('R')), 
                                 
                                 (estadoDejarNum1,'|')->(estadoDejarNum1,'|',new Direccion('R')), 
                                 (estadoDejarNum1,'F')->(estadoDejarNum1,'F',new Direccion('R')), 
                                 (estadoDejarNum1,'/')->(estadoDejarNum1,'/',new Direccion('R')), 
                                 (estadoDejarNum1,'-')->(estadoDejarNum1,'-',new Direccion('R')), 
                                 (estadoDejarNum1,'X')->(estadoDejarNum1,'X',new Direccion('R')), 
                                 
                                 (estadoMover0Num2, '-')-> (estadoMover0Num2, '-', new Direccion('R')),
                                 (estadoMover0Num2, 'F')-> (estadoMover0Num2, 'F', new Direccion('R')),
                                 
                                 (estadoMover1Num2, '-')-> (estadoMover1Num2, '-', new Direccion('R')),
                                 (estadoMover1Num2, 'F')-> (estadoMover1Num2, 'F', new Direccion('R')),
                                 
                                 (sumar0, 'X')-> (sumar0, 'X', new Direccion('R')),
                                 (sumar0, '/')-> (sumar0, '/', new Direccion('R')),
                                 (sumar0, '|')-> (sumar0, '|', new Direccion('R')),
                                 (sumar0, '-')-> (sumar0, '-', new Direccion('R')),
                                 (sumar0, 'F')-> (sumar0, 'F', new Direccion('R')),
                                 
                                 (sumar1, 'X')-> (sumar1, 'X', new Direccion('R')),
                                 (sumar1, '/')-> (sumar1, '/', new Direccion('R')),
                                 (sumar1, '|')-> (sumar1, '|', new Direccion('R')),
                                 (sumar1, 'F')-> (sumar1, 'F', new Direccion('R')),
                                 (sumar1, '-')-> (sumar1, '-', new Direccion('R')),
                                 
                                 (acarreo1, 'X')-> (acarreo1, 'X', new Direccion('R')),
                                 (acarreo1, '/')-> (acarreo1, '/', new Direccion('R')),
                                 (acarreo1, '|')-> (acarreo1, '|', new Direccion('R')),
                                 (acarreo1, 'F')-> (acarreo1, 'F', new Direccion('R')),
                                 (acarreo1, '-')-> (acarreo1, '-', new Direccion('R')),
                                 
                                 (estadoBuscarNum2, 'F')-> (estadoBuscarNum2, 'F', new Direccion('R')),
                                 (estadoBuscarNum2, '-')-> (estadoBuscarNum2, '-', new Direccion('R')),
                                 
                                 (estadoBuscarInicioNum2, 'F')-> (estadoBuscarInicioNum2, 'F', new Direccion('R')),
                                 (estadoBuscarInicioNum2, '-')-> (estadoBuscarInicioNum2, '-', new Direccion('R')),
                                 
                                 (buscarBarraFinal, 'F')-> (buscarBarraFinal, 'F', new Direccion('R')),
                                 (buscarBarraFinal, '-')-> (buscarBarraFinal, '-', new Direccion('R')),
                                 
                                 (reacomodarBarraFinal, 'X')-> (reacomodarBarraFinal, 'X', new Direccion('R')),
                                 (reacomodarBarraFinal, '/')-> (reacomodarBarraFinal, '/', new Direccion('R')),
                                 (reacomodarBarraFinal, '|')-> (reacomodarBarraFinal, '|', new Direccion('R')),
                                 (reacomodarBarraFinal, '-')-> (reacomodarBarraFinal, '-', new Direccion('R')),
                                 (reacomodarBarraFinal, 'F')-> (reacomodarBarraFinal, 'F', new Direccion('R')),
                                 
                                 (nuevo0, '0')-> (nuevo0, '0', new Direccion('R')),
                                 (nuevo0, '1')-> (nuevo0, '1', new Direccion('R')),
                                 (nuevo0, 'B')-> (nuevo0, 'B', new Direccion('R')),
                                 (nuevo0, 'X')-> (nuevo0, 'X', new Direccion('R')),
                                 (nuevo0, 'F')-> (nuevo0, 'F', new Direccion('R')),
                                 (nuevo0, '-')-> (nuevo0, '-', new Direccion('R')),
                                 (nuevo0, '/')-> (nuevo0, '/', new Direccion('R')),
                                 
                                 (nuevo1, '0')-> (nuevo1, '0', new Direccion('R')),
                                 (nuevo1, '1')-> (nuevo1, '1', new Direccion('R')),
                                 (nuevo1, 'B')-> (nuevo1, 'B', new Direccion('R')),
                                 (nuevo1, 'X')-> (nuevo1, 'X', new Direccion('R')),
                                 (nuevo1, 'F')-> (nuevo1, 'F', new Direccion('R')),
                                 (nuevo1, '-')-> (nuevo1, '-', new Direccion('R')),
                                 (nuevo1, '/')-> (nuevo1, '/', new Direccion('R')),
                                 
                                 (nuevoB, '0')-> (nuevoB, '0', new Direccion('R')),
                                 (nuevoB, '1')-> (nuevoB, '1', new Direccion('R')),
                                 (nuevoB, 'B')-> (nuevoB, 'B', new Direccion('R')),
                                 (nuevoB, 'X')-> (nuevoB, 'X', new Direccion('R')),
                                 (nuevoB, '-')-> (nuevoB, '-', new Direccion('R')),
                                 (nuevoB, 'F')-> (nuevoB, 'F', new Direccion('R')),
                                 (nuevoB, '/')-> (nuevoB, '/', new Direccion('R')),
                                 
     )
                                
                            
                        
     val maquina=new MaquinaTuring(transiciones)
      
   
     val listaChar=List('B', '1', '1', '1','0','/','B','1','1','1','1','/','B','B','B','B','B','|') 
     val cinta=new Cinta(listaChar,4) 
     assert(maquina.procesar(estadoNum1,cinta).toString()=="(Estado actual: <f> y es final,Lista: List(B, X, X, X, X, |, B, X, X, X, X, B, |, 1, 1, 1, 0, 1) | Posicion Actual: 4)")
   }
    
    
    
    
    
    
  
}