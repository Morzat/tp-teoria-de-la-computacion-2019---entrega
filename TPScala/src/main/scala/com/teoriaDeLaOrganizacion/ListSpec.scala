package com.teoriaDeLaOrganizacion

import AFND._

class ListSpec extends UnitSpec {
    
    "After insert an element in List" must "be inside the List" in {
      
      val myList: List[Int] = List()
      val myInput: Int = 1
      val x = myInput :: myList
      
      assert(x.contains(myInput))
      
    }
    
    "Max() over empty list of type Int" must "throw UnsupportedOperationException" in {
      
      val myList: List[Int] = List()
      
      assertThrows[UnsupportedOperationException]{
        myList.max
      }
      
    }
    
    "An empty string" must "be recognized if the initial state is a Final State" in {
    
      val afnd = AFND.create()
      val emptyString = ""
      assert(afnd.procesar(emptyString))
    
    }
    
    "A string with characters that are not in the alphabet of the AFND" must "not be recognized" in {
    
      val afnd = AFND.create()
      val invalidString = "casa"
      assert(!afnd.procesar(invalidString))
    
    }
    
}