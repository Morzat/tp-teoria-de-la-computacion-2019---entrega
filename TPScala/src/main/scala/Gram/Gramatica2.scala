package Gram

object Gramatica2 
{

  def procesar(entrada: List[Char]): String=
  {
    
    if (entrada.length<3) // evito los casos borde, ya que la gramatica tampoco acepta cadenas vacias
    {
      "Error de parsing"
    }
    else
    {
       val aux=S(0,entrada)

      if (S(0,entrada)==entrada.length)
        "Exito"
      else
        "Error de parsing"
    }
    
   
      
  }
  
   def S(indice: Int,entrada: List[Char]): Int =
   {
         
         if (indice<entrada.length && entrada.apply(indice)=='c')
         {
           val aux=A(indice+1,entrada)
           if (aux<entrada.length && entrada.apply(aux)=='b')
             aux+1
           else
             aux
         }
         else
           A(indice,entrada)
        
        
         
     
     
   }
   def A(indice: Int,entrada: List[Char]): Int =
   {
    
     if (indice<entrada.length && entrada.apply(indice)=='a')
     {
       val aux=indice+1
       if (aux<entrada.length && entrada.apply(aux)=='b')
       {
         
         aux+1
       }
       else
         aux
       
     }
     else
       indice
     
   }
}
