package Gram

import com.teoriaDeLaOrganizacion._

class ListSpec extends UnitSpec 
{
  
    "Ingreso id "must " tiene que devolver true "in 
    {
      assert(Gramatica1.procesar(List('i','d'))=="Exito")
    }
    
     "Ingreso (id) "must " tiene que devolver true "in 
    {
      assert(Gramatica1.procesar(List('(','i','d',')'))=="Exito")
    }
     
     
      "Ingreso (id+id) "must " tiene que devolver true "in 
    {
      assert(Gramatica1.procesar(List('(','i','d','+','i','d',')'))=="Exito")
    }
      
       "Ingreso (id*id) "must " tiene que devolver true "in 
    {
      assert(Gramatica1.procesar(List('(','i','d','*','i','d',')'))=="Exito")
    }
        "Ingreso ((id)) "must " tiene que devolver true "in 
    {
      assert(Gramatica1.procesar(List('(','(','i','d',')',')'))=="Exito")
    }
        "Ingreso ((ad)) "must " tiene que devolver false "in 
    {
      assert(Gramatica1.procesar(List('(','(','a','d',')',')'))=="Error de parsing")
    }    
        
          "Ingreso ((id) "must " tiene que devolver false "in 
    {
      assert(Gramatica1.procesar(List('(','(','i','d',')'))=="Error de parsing")
    }    
           "Ingreso  i"must " tiene que devolver false "in 
    {
      assert(Gramatica1.procesar(List('i'))=="Error de parsing")
    }          
      
    //GRAMATICA 2
           
            "Ingreso  cabb"must " tiene que devolver true "in 
    {
      assert(Gramatica2.procesar(List('c','a','b','b'))=="Exito")
    }              
              "Ingreso  cab"must " tiene que devolver true "in 
    {
      assert(Gramatica2.procesar(List('c','a','b'))=="Exito")
    }     
            "Ingreso  cxb"must " tiene que devolver false "in 
    {
      assert(Gramatica2.procesar(List('c','x','b'))=="Error de parsing")
    }   
                    "Ingreso  a"must " tiene que devolver false "in 
    {
      assert(Gramatica2.procesar(List('a'))=="Error de parsing")
    }  
                    
}
