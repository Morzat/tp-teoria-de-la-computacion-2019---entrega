package AFND

import scala.util.matching.Regex

object AFND extends App {
  
  /*
    Lee el archivo pasado como parámetro en el constructor de la clase y genera el AFND.
   */
  def create(): AFND = {
    val elArchivo = scala.io.Source.fromFile("definition.txt")
    val lineas = try elArchivo.getLines().toList finally elArchivo.close()

//    lineas.foreach(println(_))

    // Crear alfabeto
    val alfabeto = lineas(0).toList.filter(x => (x != ',' & x != ' '))

    // Crear total estados
    val cantEstados = lineas(1).toInt

    def generarEstados(acum: Int, finales: List[Char]): List[Estado] = {
      if (acum == 0)
        Nil
      else if (finales.contains((acum + '0').toChar))
        new EstadoFinal(acum) :: generarEstados(acum-1, finales)
      else
        new Estado(acum) :: generarEstados(acum-1, finales)
    }

    val finales = lineas(2).toList.filter(x => (x != ',' & x != ' '))
    val estados = generarEstados(cantEstados, finales)

    // Asignar estados finales
    val estadosFinales = estados.filter(_.isInstanceOf[EstadoFinal]).asInstanceOf[List[EstadoFinal]]

    // Crear transiciones
    val transiciones = lineas.slice(3, lineas.size)

    def generarTransiciones(list: List[String]): List[FnTransición] = {

      def generarTransicionesRec(list: List[String], acum: Int = 0): List[FnTransición] = {

        if (acum == list.size)
          Nil
        else {
          
          val actual = list(acum).toList.filter(x => (x != ',' & x != ' ' & x != '-' & x != '>'))
          val estadoOrigen = estados.find(_.nombre == (actual(0).toInt - '0'.toInt)).get
          val input = actual(1)
          val estadoDestino = estados.find(_.nombre == (actual(2).toInt - '0'.toInt)).get
          new FnTransición(estadoOrigen, input, estadoDestino) :: generarTransicionesRec(list, acum+1)
        }

      }

      generarTransicionesRec(list)
    }

    val fnTransición = generarTransiciones(transiciones)


    new AFND(estados, alfabeto, fnTransición, estados(estados.size-1), estadosFinales)
  }

  /** Methods **/

  class AFND (val estados: List[Estado], val alfabeto: List[Char], val fnsTransición: List[FnTransición],
              val estadoInicial: Estado, val estadosFinales: List[EstadoFinal]) {

    
    override def toString: String = "\nAlfabeto: " + alfabeto.toString() +
                                    "\nEstados: " + (estados.size) +
                                    "\nEstado Inicial: " + estadoInicial.toString() +
                                    "\nEstados Finales: " + estadosFinales.toString() +
                                    "\nTransiciones:\n" + fnsTransición.mkString("\n")
    
    def procesar(w: String): Boolean = {

      def proc(actuales: List[Estado], w: String): List[Estado] = {
        
          if (w.isEmpty)
            actuales
            
          else {
            val estadosLlegada = fnsTransición.filter(f => actuales.contains(f.estadoInput) && f.símboloInput == w.head).map(_.estadoOutput)
            proc(estadosLlegada, w.tail)
          }
          
      }

      return proc(List(estadoInicial), w).exists(e => e.isInstanceOf[EstadoFinal])
    }
    

  }

  class Estado(val nombre: Int) {

    override def toString = nombre.toString

  }

  class EstadoFinal(nombre: Int) extends Estado (nombre) {

  }

  class FnTransición(val estadoInput: Estado, val símboloInput: Char, val estadoOutput: Estado) {

    override def toString: String = estadoInput.nombre + ", " + símboloInput + " -> " + estadoOutput.nombre

  }

  /** Execute **/

  val test1 = "Hello"
  val test2 = "Casa"
  val test3 = "dasa"
  val test4 = "a"
  val test5 = "d"
  val test6 = "ad"
  val test7 = "ada"
  val test8 = "aD"
  
  val test = AFND.create()
//  println("\n" + test.toString() + "\n")
  println(test1 + ": " + test.procesar(test1))
  println(test2 + ": " + test.procesar(test2))
  println(test3 + ": " + test.procesar(test3))
  println(test4 + ": " + test.procesar(test4))
  println(test5 + ": " + test.procesar(test5))
  println(test6 + ": " + test.procesar(test6))
  println(test7 + ": " + test.procesar(test7))
  println(test8 + ": " + test.procesar(test8))


}
